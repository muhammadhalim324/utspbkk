<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ProsesController extends Controller
{

public function create(){
    return view('welcome');
}

function view021190102(){
    return view('create021190102');
}

public function proses(Request $request){
    $data = array();
    $data['npm'] = $request->NPM;
    $data['nama'] = $request->nama;
    $data['program'] = $request->programstudi;
    $data['NOHP'] = $request->NOHP;
    $data['TempatTanggalLahir'] = $request->TempatTanggalLahir;
    $data['jeniskelamin'] = $request->jeniskelamin;
    $data['Agama'] = $request->Agama;
    
    return view('view021190102', ['data' => $data]);
}

}