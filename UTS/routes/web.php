<?php

use Illuminate\Support\Facades\Route;
use App\Http\controllers\ProsesController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/create021190102', [ProsesController::class,'view021190102']);
Route::post('/proses', [ProsesController::class, 'proses']);
